//
//  GridSolver.h
//  GridSolver
//
//  Created by Chris Amanse on 7/10/14.
//  Copyright (c) 2014 Joe Christopher Paul Amanse. All rights reserved.
//

typedef bool boolean;

enum GridSolverMovement {
    GridSolverMovementUp = 1,
    GridSolverMovementDown,
    GridSolverMovementLeft,
    GridSolverMovementRight
};

class GridSolver {
    int numberOfRows;
    int numberOfCols;
    int startingRow;
    int startingCol;
    int targetRow;
    int targetCol;
    
    boolean **grid;
public:
    GridSolver();
    GridSolver(int,int);
    
    int area();
    
    int maxRow() {return numberOfRows-1;}
    int maxColumn() {return numberOfCols-1;}
    
    boolean isPointValid(int,int);
    boolean setStartingPoint(int,int);
    boolean setTargetPoint(int,int);
    
    boolean valueAtPoint(int,int);
    boolean setTrueAtPoint(int,int);
    boolean setFalseAtPoint(int,int);
    
    GridSolverMovement * getMovements();
};

#pragma mark - DEBUG

void logGrid(GridSolver);