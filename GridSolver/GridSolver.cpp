//
//  GridSolver.cpp
//  GridSolver
//
//  Created by Chris Amanse on 7/10/14.
//  Copyright (c) 2014 Joe Christopher Paul Amanse. All rights reserved.
//

#include <stdio.h>
#include "GridSolver.h"

GridSolver::GridSolver() {
    numberOfRows = 3;
    numberOfCols = 3;
    startingRow = 0;
    startingCol = 0;
}

GridSolver::GridSolver(int rows, int cols) {
    numberOfRows = rows;
    numberOfCols = cols;
    startingRow = 0;
    startingCol = 0;
    
    // Create Dynamic 2D Array
    grid = 0;
    
    // Setup for 2D Array - create first column (technically create an array of pointers)
    grid = new boolean*[rows];
    
    // Then create rows for each column (technically create an array)
    for (int i=0; i<cols; i++) {
        grid[i] = new boolean[cols];
    }
    
    for (int row=0; row<rows; row++) {
        for (int col=0; col<cols; col++) {
            grid[row][col] = 0;
        }
    }
}

int GridSolver::area() {
    return numberOfRows*numberOfCols;
}

boolean GridSolver::isPointValid(int row, int col) {
    return (row < numberOfRows && col < numberOfCols && row >= 0 && col >= 0);
}

boolean GridSolver::setStartingPoint(int row, int col) {
    if (this->isPointValid(row, col)) {
        startingRow = row;
        startingCol = col;
        return true;
    }
    return false;
}

boolean GridSolver::setTargetPoint(int row, int col) {
    if (this->isPointValid(row, col)) {
        targetRow = row;
        targetCol = col;
        return true;
    }
    return false;
}

boolean GridSolver::valueAtPoint(int row, int col) {
    if (this->isPointValid(row, col)) {
        return grid[row][col];
    }
    return false;
}

boolean GridSolver::setTrueAtPoint(int row, int col) {
    if (this->isPointValid(row, col)) {
        grid[row][col] = 1;
        return true;
    }
    return false;
}

boolean GridSolver::setFalseAtPoint(int row, int col) {
    if (this->isPointValid(row, col)) {
        grid[row][col] = 0;
        return true;
    }
    return false;
}

GridSolverMovement * GridSolver::getMovements() {
    GridSolverMovement *movements = new GridSolverMovement[this->area()];
    int numberOfMoves = 0;
    
    int currentRow = startingRow;
    int currentCol = startingCol;
    
    // Shortest distance - NO EVADING OF NODES
    
    // Vertical movement
    while (currentRow < targetRow) {
        currentRow++;
        movements[numberOfMoves] = GridSolverMovementUp;
        numberOfMoves++;
    }
    while (currentRow > targetRow) {
        currentRow--;
        movements[numberOfMoves] = GridSolverMovementDown;
        numberOfMoves++;
    }
    
    // Horizontal
    while (currentCol < targetCol) {
        currentCol++;
        movements[numberOfMoves] = GridSolverMovementRight;
        numberOfMoves++;
    }
    while (currentCol > targetCol) {
        currentCol--;
        movements[numberOfMoves] = GridSolverMovementLeft;
        numberOfMoves++;
    }
    
    return movements;
}

#pragma mark - DEBUG

void logGrid(GridSolver aGrid) {
    int upperBoundCol = aGrid.maxColumn()+1;
    
    printf("\n**");
    for (int col=0; col < upperBoundCol; col++) {
        printf("**");
    }
    printf("*\n");
    
    for (int row = aGrid.maxRow(); row>=0; row--) {
        printf("* ");
        for (int col=0; col < upperBoundCol; col++) {
            if (aGrid.valueAtPoint(row, col)) {
                printf("X ");
            } else {
                printf("_ ");
            }
        }
        printf("*\n");
    }
    printf("**");
    for (int col=0; col < upperBoundCol; col++) {
        printf("**");
    }
    printf("*\n");
}
