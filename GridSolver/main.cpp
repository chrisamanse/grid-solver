//
//  main.cpp
//  GridSolver
//
//  Created by Chris Amanse on 7/10/14.
//  Copyright (c) 2014 Joe Christopher Paul Amanse. All rights reserved.
//

#include <iostream>
#include "GridSolver.h"

int main(int argc, const char * argv[]) {
    printf("Hello, World!\n");
    
    GridSolver myGrid(4,6);
    printf("%i\n", myGrid.area());
    
    if (myGrid.setStartingPoint(0,0)) {
        printf("Valid starting point\n");
    }
    
    if (myGrid.setTargetPoint(1,3)) {
        printf("Valid target point\n");
    }
    
    myGrid.setTrueAtPoint(2, 2);
    myGrid.setTrueAtPoint(1, 3);
    myGrid.setTrueAtPoint(2, 4);
    
    printf("%i\n", myGrid.valueAtPoint(3, 5));
    logGrid(myGrid);
    
    GridSolverMovement *movements = myGrid.getMovements();
    
    for (int i=0; i < myGrid.area(); i++) {
        if (movements[i]) {
            switch (movements[i]) {
                case GridSolverMovementUp:
                    printf("Up,");
                    break;
                case GridSolverMovementDown:
                    printf("Down,");
                    break;
                case GridSolverMovementLeft:
                    printf("Left,");
                    break;
                case GridSolverMovementRight:
                    printf("Right,");
                    break;
                default:
                    break;
            }
        } else {
            printf("END\nNumber of moves %i\n",i);
            break;
        }
    }
    
    return 0;
}